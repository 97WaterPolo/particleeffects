package us.siglerdev.sigler.manager;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;

import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.sigler.MySQLUtil;
import us.siglerdev.sigler.ParticleTrails;

public class TrailRepository extends MySQLUtil
{
    public TrailRepository(){
        tableName = "particletrails_active";
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        map.put("player_id", "varchar(40),");
        map.put("particle_name", "varchar(40), UNIQUE(player_id,particle_name)");
        makeTable(map);
    }

    public void addActiveParticleEffect(final UUID id, final String particleName){
        new BukkitRunnable()
        {

            public void run()
            {
                runStatement("INSERT INTO " + tableName + " (player_id,particle_name) VALUES (?,?) ON DUPLICATE KEY UPDATE player_id = player_id", id.toString(),particleName);

            }
        }.runTaskAsynchronously(ParticleTrails.i);
    }

    public void removeActiveParticleEffect(final UUID id, final String particleName){
        new BukkitRunnable()
        {

            public void run()
            {
                runStatement("DELETE FROM " + tableName + " WHERE `player_id` = ? AND `particle_name` = ?", id.toString(), particleName);

            }
        }.runTaskAsynchronously(ParticleTrails.i);
    }

    public List<String> getActiveEffects(final UUID id){
        final List<String> list = new ArrayList<String>();
        new BukkitRunnable()
        {

            public void run()
            {
                List<Object> vals = queryDatabase("SELECT `particle_name` FROM " + tableName + " WHERE `player_id` = ?", id.toString());
                if (vals != null)
                    for (Object o : vals)
                        if (o != null)
                            list.add(o.toString());

            }
        }.runTaskAsynchronously(ParticleTrails.i);
        return list;
    }


}
