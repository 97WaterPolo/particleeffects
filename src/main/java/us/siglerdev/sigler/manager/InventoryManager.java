package us.siglerdev.sigler.manager;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import us.siglerdev.sigler.ParticlePlayer;
import us.siglerdev.sigler.ParticleTrails;
import us.siglerdev.sigler.particle.ParticleItem;
import us.siglerdev.sigler.utils.Items;
import us.siglerdev.sigler.utils.ParticleLocation;

/**
 * Created by Alexander on 1/27/2016.
 */
public class InventoryManager implements Listener {
    private static MessageManager message;
    public InventoryManager(){
        this.message = MessageManager.i;
    }

    static int[] glassSlots = new int[]{1,10,19,28,36,37,46,50,51,52,53}; //18 is leggings slot
    public void openParticleInventory(Player player){
        ParticlePlayer particlePlayer = ParticleTrails.i.getParticlePlayer(player.getUniqueId());
        Inventory inventory = Bukkit.createInventory(player,54, ChatColor.translateAlternateColorCodes('&',message.get("View.InventoryName").toString()));
        updateArmorItems(inventory, particlePlayer, particlePlayer.getParticleLocation());
        inventory.setItem(45,Items.CLEAR);
        for (int x : glassSlots)
            inventory.setItem(x, Items.GLASS);
        for (ParticleItem particleItem : ParticleTrails.i.getParticleItemList()) {
            inventory.addItem(particleItem.getItemstack((player.hasPermission(particleItem.getPermission())),
                    particlePlayer.hasEffectActive(particleItem)));
        }
        player.openInventory(inventory);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event){
        if (!(event.getWhoClicked() instanceof Player))
            return;
        final Player player = (Player) event.getWhoClicked();
        Inventory inv = event.getInventory();
        final ItemStack is = event.getCurrentItem();
        int slot = event.getSlot();
        if (!inv.getName().equalsIgnoreCase(org.bukkit.ChatColor.translateAlternateColorCodes('&',MessageManager.i.get("View.InventoryName").toString())))
            return;
        if (is == null || is.getType() == Material.AIR)
            return;
        event.setCancelled(true); //Cancel all click events within this inventory.

        if (!ParticleTrails.i.getParticlePlayer(player.getUniqueId()).canClick())
            return;
        ParticleTrails.i.getParticlePlayer(player.getUniqueId()).triggerClickCooldown();
        if(event.getRawSlot() < event.getInventory().getSize()) {//It is in the top inventory
            for (int x : glassSlots)
                if (x == slot)
                    return;
            ParticlePlayer particlePlayer = ParticleTrails.i.getParticlePlayer(player.getUniqueId());
            if (slot == 0) { //Helmet
                particlePlayer.setParticleLocation(ParticleLocation.HEAD);
            }else if (slot == 9){ //Aura
                particlePlayer.setParticleLocation(ParticleLocation.AURA);
            }else if (slot == 18){ //Trail
                particlePlayer.setParticleLocation(ParticleLocation.TRAIL);
            }else if (slot == 27){ //Feet
                particlePlayer.setParticleLocation(ParticleLocation.FEET);
            } else if (slot == 45){ //Redstone Clear block.
                particlePlayer.disableAllEffects();
                new BukkitRunnable(){
                    @Override
                    public void run() {
                        player.closeInventory();
                        new BukkitRunnable(){
                            @Override
                            public void run() {
                                openParticleInventory(player);
                            }
                        }.runTaskLater(ParticleTrails.i,1);
                    }
                }.runTaskLater(ParticleTrails.i,1);
                return;
            }else{
                ParticleItem particleItem = ParticleTrails.i.getParticleItem(is.getItemMeta().getDisplayName());
                if (particlePlayer.hasEffectActive(particleItem)){
                    particlePlayer.removeParticleEffect(particleItem.getParticleType());
                }else {
                    if (player.hasPermission(particleItem.getPermission())) {
                        if (particlePlayer.canActivateEffect()) {
                            particlePlayer.startParticleEffect(particleItem.getParticleType());
                        } else {
                            player.sendMessage(MessageManager.i.getMessage("ToManyParticles", particleItem.getName()));
                        }
                    }else{
                        if (ParticleTrails.i.isSoundEnabled())
                            player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_LAND,1,1);
                        player.sendMessage(MessageManager.i.getMessage("NoPermission"));
                    }
                }

                if (particleItem != null) { //Updates the item to reflect changes we made
                    inv.setItem(slot, particleItem.getItemstack((player.hasPermission(particleItem.getPermission())),
                            particlePlayer.hasEffectActive(particleItem)));
                }
            }
            updateArmorItems(inv, particlePlayer, particlePlayer.getParticleLocation());

        }

    }

    public void updateArmorItems(Inventory inventory, ParticlePlayer particlePlayer, ParticleLocation particleLocation){
        inventory.setItem(0, particlePlayer.getActiveLocation(ParticleLocation.HEAD, message.getMessage("HeadLocation"),
                Items.ON_HELMET, Items.OFF_HELMET));
        inventory.setItem(9, particlePlayer.getActiveLocation(ParticleLocation.AURA, message.getMessage("AuraLocation"),
                Items.ON_CHESTPLATE, Items.OFF_CHESTPLATE));
        inventory.setItem(18, particlePlayer.getActiveLocation(ParticleLocation.TRAIL, message.getMessage("TrailLocation"),
                Items.ON_LEGGINGS, Items.OFF_LEGGINGS));
        inventory.setItem(27, particlePlayer.getActiveLocation(ParticleLocation.FEET, message.getMessage("FeetLocation"),
                Items.ON_BOOTS, Items.OFF_BOOTS));
        if (particleLocation != null)
            if (particleLocation == ParticleLocation.HEAD)
                inventory.getItem(0).addUnsafeEnchantment(ParticleTrails.fakeEnchant,1);
            else if (particleLocation == ParticleLocation.AURA)
                inventory.getItem(9).addUnsafeEnchantment(ParticleTrails.fakeEnchant,1);
            else if (particleLocation == ParticleLocation.TRAIL)
                inventory.getItem(18).addUnsafeEnchantment(ParticleTrails.fakeEnchant,1);
            else if (particleLocation == ParticleLocation.FEET)
                inventory.getItem(27).addUnsafeEnchantment(ParticleTrails.fakeEnchant,1);
    }


}
