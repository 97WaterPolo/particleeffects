package us.siglerdev.sigler.manager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import us.siglerdev.sigler.ParticlePlayer;
import us.siglerdev.sigler.particle.Particle;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Alexander on 1/26/2016.
 */
public class PlayerManager implements Listener{
    private List<ParticlePlayer> players;
    public PlayerManager(){
        players = new ArrayList<ParticlePlayer>();
    }

    public ParticlePlayer addPlayer(UUID id){
        for (ParticlePlayer particlePlayer : players)
            if (particlePlayer.getId().equals(id))
                return null;
        ParticlePlayer particlePlayer = new ParticlePlayer(id);
        players.add(particlePlayer);
        return particlePlayer;
    }

    public void removePlayer(UUID id){
        for (ParticlePlayer particlePlayer : players){
            if (particlePlayer.getId().equals(id)){
                particlePlayer.disable();
                players.remove(particlePlayer);
                break;
            }
        }
    }

    public ParticlePlayer getParticlePlayer(UUID id){
        for (ParticlePlayer particlePlayer : players)
            if (particlePlayer.getId().equals(id))
                return particlePlayer;
        return addPlayer(id);
    }

    @EventHandler
    public void onJoin(AsyncPlayerPreLoginEvent event){
        addPlayer(event.getUniqueId());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event){
        removePlayer(event.getPlayer().getUniqueId());
    }

}
