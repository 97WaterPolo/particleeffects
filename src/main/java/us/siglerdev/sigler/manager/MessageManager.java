package us.siglerdev.sigler.manager;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import us.siglerdev.sigler.ParticleTrails;

import java.util.List;

/**
 * Created by Alexander on 1/27/2016.
 */
public class MessageManager {

    private YamlConfiguration messages;
    public static MessageManager i;
    public MessageManager(YamlConfiguration yamlConfiguration){
        this.messages = yamlConfiguration;
        this.i = this;
    }

    public List<String> getStringList(String path){
        return messages.getStringList(path);
    }

    public String getMessage(String s){
        if (messages.contains("Messages."+s))
            return ChatColor.translateAlternateColorCodes('&', messages.getString("Messages."+s));
        return "CHANGE ME!";
    }

    public String getMessage(String s, String effectName){
        return getMessage(s).replace("%effect%", effectName);
    }

    public Object get(String s){
        return messages.get(s);
    }

    public YamlConfiguration getConfiguration(){
        return messages;
    }

}
