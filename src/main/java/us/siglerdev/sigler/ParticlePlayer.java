package us.siglerdev.sigler;

import java.util.*;

import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import us.siglerdev.sigler.manager.MessageManager;
import us.siglerdev.sigler.particle.Particle;
import us.siglerdev.sigler.particle.ParticleEffect;
import us.siglerdev.sigler.particle.ParticleItem;
import us.siglerdev.sigler.utils.ParticleLocation;
import us.siglerdev.sigler.utils.Utils;

public class ParticlePlayer
{
    private UUID id;
    private ParticleLocation particleLocation;
    private ParticleTrails plugin;
    private List<String> activeParticleEffects;
    private List<Particle> active;
    private boolean loaded;
    private long lastClick;
    public ParticlePlayer(UUID player_id){
        this.id = player_id;
        this.plugin = ParticleTrails.i;
        this.loaded = false;
        this.lastClick = System.currentTimeMillis();
        particleLocation = ParticleLocation.valueOf(MessageManager.i.get("DefaultLocation").toString());
        activeParticleEffects = new ArrayList<String>();
        active = new ArrayList<Particle>();
        loadPreferences();
        new BukkitRunnable(){
            int attempts = 0;
            @Override
            public void run() {
                if (isLoaded()) {
                    if (plugin.isMySQLEnabled() && (attempts < 10 && activeParticleEffects.size() == 0))
                        attempts = attempts + 0; //A filler method in case it has a slight delay.
                    else if (id != null){
                        try {
                            if (Bukkit.getPlayer(id) != null) {
                                if (activeParticleEffects.size() == 0)
                                    getPlayer().sendMessage(MessageManager.i.getMessage("NoActiveEffectLogin"));
                                else
                                    getPlayer().sendMessage(MessageManager.i.getMessage("LoginMessage"));
                                for (String s : activeParticleEffects)
                                    updateEffects(ParticleEffect.ParticleType.valueOf(s));
                                this.cancel();
                                return;
                            }
                        }catch(Exception e){
                            ParticleTrails.i.getLogger().info("Catching an NPE. ID: " + id);
                        }
                    }
                }
                attempts++;
                //ParticleTrails.i.getLogger().info("Attempting to load data for " + id + " try: " + attempts);
                if (attempts > 15){
                    this.cancel();
                    ParticleTrails.i.getLogger().info("Could not load the player data for " + id + " or player is offline!");
                }
            }
        }.runTaskTimer(ParticleTrails.i, 3,3);
    }

    private void updateEffects(ParticleEffect.ParticleType particleType){
        if (activeParticleEffects.size() == active.size())
            return;
        if (active.size() > activeParticleEffects.size()){ //One needs to be removed
            for (Particle particle : active){
                if (particle.getType().equalsIgnoreCase(particleType.toString())){
                    particle.cancel();
                    active.remove(particle);
                    break;
                }
            }
        }else{ //A new one was added
            active.add(new Particle(id, ParticleTrails.i.getParticleItem(particleType).getParticleEffect(), particleLocation));
        }
        checkPermission();
    }

    public void startParticleEffect(ParticleEffect.ParticleType particleType){
        if (activeParticleEffects.contains(particleType.toString()))
            return;
        getPlayer().sendMessage(MessageManager.i.getMessage("Enabled", (ParticleTrails.i.getParticleItem(particleType)).getName()));
        if (ParticleTrails.i.isSoundEnabled())
            getPlayer().playSound(getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 0);
        addEffect(particleType.toString());
        updateEffects(particleType);
    }

    public void removeParticleEffect(ParticleEffect.ParticleType particleType){
        if (!activeParticleEffects.contains(particleType.toString()))
            return;
        getPlayer().sendMessage(MessageManager.i.getMessage("Disabled", (ParticleTrails.i.getParticleItem(particleType)).getName()));
        if (ParticleTrails.i.isSoundEnabled())
            getPlayer().playSound(getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 0);
        removeEffect(particleType.toString());
        updateEffects(particleType);
    }

    public void restart(){
        if (active != null)
            for (Particle particle : active) {
                particle.cancel();
                active.remove(particle);
            }
        if (activeParticleEffects != null)
            for (String s : activeParticleEffects)
                updateEffects(ParticleEffect.ParticleType.valueOf(s));

    }

    public void loadPreferences(){
        if (plugin.isMySQLEnabled()){
            activeParticleEffects = plugin.getTrailRepository().getActiveEffects(id);
        }else{
            if (plugin.dataConfig.contains(id.toString()))
                activeParticleEffects = plugin.dataConfig.getStringList(id.toString());
        }
        loaded = true;
    }

    private void addEffect(String particleName){
        activeParticleEffects.add(particleName);
        if (plugin.isMySQLEnabled()){
            plugin.getTrailRepository().addActiveParticleEffect(id, particleName);
        }else{
            plugin.dataConfig.set(id.toString(), activeParticleEffects);
            plugin.saveCustomConfig();
        }
    }

    private void removeEffect(String particleName){
        activeParticleEffects.remove(particleName);
        if (plugin.isMySQLEnabled()){
            plugin.getTrailRepository().removeActiveParticleEffect(id, particleName);
        }else{
            if (activeParticleEffects.size() > 0)
                plugin.dataConfig.set(id.toString(), activeParticleEffects);
            else
                plugin.dataConfig.set(id.toString(), null);
            plugin.saveCustomConfig();
        }
    }

    public ItemStack getActiveLocation(ParticleLocation loc, String displayName, ItemStack... items){
        List<String> values = new ArrayList<String>();
        ItemStack is = null;
        for (Particle particle : active)
            if (particle.getParticleLocation() == loc)
                values.add("- " + WordUtils.capitalize(particle.getType().toLowerCase().replace("_"," ")));
        if (values.size() > 0)
            is = items[0];
        else
            is = items[1];
        Utils.clearItemStack(is);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
        im.setLore(values);
        is.setItemMeta(im);
        return is;
    }

    private void checkPermission(){
        List<String> particleRemoval = new ArrayList<String>(activeParticleEffects);
        for (String s : particleRemoval){
            String permission = ParticleTrails.i.getParticleItem(ParticleEffect.ParticleType.valueOf(s)).getPermission();
            if (!getPlayer().hasPermission(permission))
                removeParticleEffect(ParticleEffect.ParticleType.valueOf(s));
        }
    }

    public int getActiveAmount(){
        return active.size();
    }

    public Player getPlayer(){
        return Bukkit.getPlayer(id);
    }

    public boolean canActivateEffect(){
        for (int x = 50; x > 0; x--)
            if (getPlayer().hasPermission("Particle.amount."+x)) {
                return (active.size() < x);
            }
        return false;
    }

    public boolean hasEffectActive(ParticleItem particleItem){
        for (String s : activeParticleEffects){
            if (particleItem.getParticleType().toString().equalsIgnoreCase(s))
                return true;
        }
        return false;
    }

    public ParticleLocation getParticleLocation(){
        return particleLocation;
    }

    public boolean canClick(){
        return (lastClick+750) < System.currentTimeMillis();

    }

    public void triggerClickCooldown(){
        lastClick = System.currentTimeMillis();
    }

    public boolean isLoaded(){
        return loaded;
    }

    public UUID getId(){
        return id;
    }

    public void setParticleLocation(ParticleLocation newParticleLocation){
        if (getPlayer().hasPermission("Particle.location."+newParticleLocation.toString().toLowerCase())) {
            this.particleLocation = newParticleLocation;
            for (Particle particle : active)
                particle.updateParticleLocation(particleLocation);
            if (ParticleTrails.i.isSoundEnabled())
                getPlayer().playSound(getPlayer().getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
            getPlayer().sendMessage(MessageManager.i.getMessage(WordUtils.capitalize(newParticleLocation.toString().toLowerCase())+"Enabled"));
        }else
            getPlayer().sendMessage(MessageManager.i.getMessage("NoPermission"));
    }

    public void disableAllEffects(){
        getPlayer().sendMessage(MessageManager.i.getMessage("AllParticlesDisabled"));
        if (ParticleTrails.i.isSoundEnabled())
            getPlayer().playSound(getPlayer().getLocation(), Sound.ENTITY_BLAZE_HURT, 1, 1);
        for (Particle particle : active)
            particle.cancel();
        List<String> list = new ArrayList<String>(activeParticleEffects);
        for (String s : list)
            removeEffect(s);
        active.clear();
        activeParticleEffects.clear();
    }

    public void disable(){
        //TODO
    }

    @Override
    public String toString(){
        return "ActiveSize: " + active.size() + "  VALID: " + activeParticleEffects.size();
    }
}
