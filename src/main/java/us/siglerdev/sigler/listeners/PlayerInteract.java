package us.siglerdev.sigler.listeners;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import us.siglerdev.sigler.ParticleTrails;
import us.siglerdev.sigler.manager.MessageManager;

/**
 * Created by Alexander on 1/31/2016.
 */
public class PlayerInteract implements Listener{
    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if (event.getItem() == null || event.getItem().getType() == Material.AIR)
            return;
        Player player = event.getPlayer();
        YamlConfiguration c = MessageManager.i.getConfiguration();
        boolean right = false;
        boolean info = false;
        if (c.getBoolean("Item.Enabled"))
            right = true;
        if (c.getBoolean("Item.Info.Enabled"))
            info = true;
        if (right){
            if (event.getItem().getType() == Material.getMaterial(c.getInt("Item.Material"))){
                if (info){
                    if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',c.getString("Item.Info.Name"))))
                        ParticleTrails.i.getInventoryManager().openParticleInventory(player);
                }else
                    ParticleTrails.i.getInventoryManager().openParticleInventory(player);
            }
        }
    }
}
