package us.siglerdev.sigler.listeners;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 * Created by Alexander on 2/5/2016.
 */
public class Hologram {
    private ArmorStand armorStand;
    private LivingEntity livingEntity;
    public Hologram(LivingEntity livingEntity){
        this.livingEntity = livingEntity;
        armorStand = (ArmorStand) livingEntity.getWorld().spawnEntity(livingEntity.getLocation(), EntityType.ARMOR_STAND);
        spawnHologram();
    }

    private void spawnHologram(){
        armorStand.setMarker(true);
        armorStand.setSmall(true);
        armorStand.setVisible(false);
        armorStand.setCustomName("Test Chicken");
        armorStand.setCustomNameVisible(true);
       // armorStand.setGravity(false);
        livingEntity.setPassenger(armorStand);
    }
}
