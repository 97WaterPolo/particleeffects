package us.siglerdev.sigler.listeners;

import us.siglerdev.sigler.MySQLUtil;

import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

public class TestMySQL extends MySQLUtil {
    public TestMySQL(){
        tableName = "db_test"; //The database 's table name
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        map.put("player_uuid", "varchar(40),"); //Create a column that is called player_uuid with varchar type
        map.put("elo", "INT");  //Create a column that is called ELO with INT type
        makeTable(map); //This will try and create the table if it does NOT exist.
    }

    public void setPlayerElo(UUID id, int value){
        runStatement("INSERT INTO " + tableName + " (player_uuid,elo) VALUES (?,?) ON DUPLICATE KEY UPDATE elo = ?", id.toString(),value,value);
    }

    public void removePlayerElo(UUID id){
        runStatement("DELETE FROM " + tableName + " WHERE `player_uuid` = ?", id.toString());
    }

    public int getElo(UUID id){
        List<Object> vals = queryDatabase("SELECT `elo` FROM " + tableName + " WHERE `player_uuid` = ?", id.toString());
        if (vals != null){ //It returns null if nothing was found
            return (int) vals.get(0);
        }else
            return 0; //The player doesn't have an ELO value.
    }

    public Connection getConnection(){
        return getOriginalConnection();
    }
}
