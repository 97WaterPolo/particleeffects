package us.siglerdev.sigler.cmds;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import us.siglerdev.sigler.ParticlePlayer;
import us.siglerdev.sigler.ParticleTrails;
import us.siglerdev.sigler.manager.MessageManager;
import us.siglerdev.sigler.particle.ParticleItem;
import us.siglerdev.sigler.utils.CommandAssist;
import us.siglerdev.sigler.utils.ParticleLocation;

import java.util.*;

/**
 * Created by Alexander on 1/29/2016.
 */
public class ParticleCommand implements CommandExecutor{
    private boolean cmdOpenInventory;
    private List<TextComponent> helpList,adminList;
    private List<String> commandValues;
    public ParticleCommand(){
        cmdOpenInventory = ParticleTrails.i.config.getBoolean("ParticleOpenInventory");
        helpList = new ArrayList<>();
        adminList = new ArrayList<>();
        createHelpMenu();

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)){
            sender.sendMessage(ChatColor.DARK_RED + "Particle console administrator commands have not been added yet.");
            return false;
        }
        Player player = (Player) sender;
        if (cmdOpenInventory) { //If true, will open inventory immediately
            ParticleTrails.i.getInventoryManager().openParticleInventory(player);
        }else{
            if (args.length == 0){
                if (MessageManager.i.getConfiguration().getBoolean("ParticleOpenInventory"))
                    ParticleTrails.i.getInventoryManager().openParticleInventory(player);
                else
                    sendHelpMenu(player);
                return false;
            }
            String realArgument = getValidArgument(args[0]);
            ParticlePlayer particlePlayer = ParticleTrails.i.getParticlePlayer(player.getUniqueId());
            switch(realArgument){
                case "inv":
                    ParticleTrails.i.getInventoryManager().openParticleInventory(player);
                    break;
                case "aura":
                    ParticleTrails.i.getParticlePlayer(player.getUniqueId()).setParticleLocation(ParticleLocation.AURA);
                    break;
                case "feet":
                    ParticleTrails.i.getParticlePlayer(player.getUniqueId()).setParticleLocation(ParticleLocation.FEET);
                    break;
                case "head":
                    ParticleTrails.i.getParticlePlayer(player.getUniqueId()).setParticleLocation(ParticleLocation.HEAD);
                    break;
                case "trail":
                    ParticleTrails.i.getParticlePlayer(player.getUniqueId()).setParticleLocation(ParticleLocation.TRAIL);
                    break;
                case "off":
                    if (player.hasPermission("Particle.admin.off")){
                        if (args.length == 2){
                            Player target = Bukkit.getPlayer(args[1]);
                            if (target == null){
                                player.sendMessage("That player is not online!");
                            }else{
                                ParticleTrails.i.getParticlePlayer(target.getUniqueId()).disableAllEffects();
                                player.sendMessage("Disabled all effects for " + target.getName());
                            }
                        }else if (args.length == 1){
                            ParticleTrails.i.getParticlePlayer(player.getUniqueId()).disableAllEffects();
                        }else
                            player.sendMessage("Wrong usage.");
                    }else
                        ParticleTrails.i.getParticlePlayer(player.getUniqueId()).disableAllEffects();
                    break;
                case "toggle":
                    if (player.hasPermission("Particle.toggle")) {
                        if (ParticleTrails.i.getToggled().contains(player.getUniqueId())) {
                            ParticleTrails.i.getToggled().remove(player.getUniqueId());
                            player.sendMessage(ChatColor.YELLOW + "You will now see particles.");
                        } else {
                            ParticleTrails.i.getToggled().add(player.getUniqueId());
                            player.sendMessage(ChatColor.GOLD + "You will no longer see particles.");
                        }
                    }else
                        player.sendMessage(MessageManager.i.getMessage("NoPermission"));
                    break;
                case "help":
                    sendHelpMenu(player);
                    break;
                case "demo":
                    startDemo(player);
                    break;
                case "reload":
                    if (player.hasPermission("Particle.admin.reload")){
                        player.sendMessage("Reloading the particle configuration!");
                        ParticleTrails.i.reloadParticleItems();
                        for (Player p : Bukkit.getOnlinePlayers()){
                            ParticleTrails.i.getParticlePlayer(p.getUniqueId()).restart();
                        }
                    }
                    break;
                case "list":
                    if (player.hasPermission("Particle.admin.show")){
                        if (args.length == 2){
                            Player target = Bukkit.getPlayer(args[1]);
                            if (target == null){
                                player.sendMessage("That player is not online!");
                            }else{
                                for (ParticleItem particleItem : ParticleTrails.i.getParticleItemList()){
                                    if (ParticleTrails.i.getParticlePlayer(target.getUniqueId()).hasEffectActive(particleItem)){
                                        player.sendMessage(particleItem.getParticleType().toString() + " - " + particleItem.getDisplayName());
                                    }
                                }
                                player.sendMessage("These are all the effects " + target.getName() + " has active!");
                            }
                        }else if (args.length == 1){
                            player.sendMessage(ChatColor.RED + "This only shows effects that you have access to!");
                            for (ParticleItem particleItem : ParticleTrails.i.getParticleItemList()) {
                                if (player.hasPermission(particleItem.getPermission())) {
                                    TextComponent textComponent = new TextComponent(
                                            new ComponentBuilder(particleItem.getName()).color(ChatColor.GOLD)
                                                    .append(": ").color(ChatColor.WHITE).create());
                                    textComponent.addExtra(getArgsFromList(particleItem.getCommands()));
                                    if (particlePlayer.hasEffectActive(particleItem))
                                        textComponent.addExtra(new TextComponent(new ComponentBuilder(" " + MessageManager.i.getMessage("Active")).create()));
                                    player.spigot().sendMessage(textComponent);
                                }
                            }
                        }else
                            player.sendMessage("Wrong usage.");
                    }else {
                        player.sendMessage(ChatColor.RED + "This only shows effects that you have access to!");
                        for (ParticleItem particleItem : ParticleTrails.i.getParticleItemList()) {
                            if (player.hasPermission(particleItem.getPermission())) {
                                TextComponent textComponent = new TextComponent(
                                        new ComponentBuilder(particleItem.getName()).color(ChatColor.GOLD)
                                                .append(": ").color(ChatColor.WHITE).create());
                                textComponent.addExtra(getArgsFromList(particleItem.getCommands()));
                                if (particlePlayer.hasEffectActive(particleItem))
                                    textComponent.addExtra(new TextComponent(new ComponentBuilder(" " + MessageManager.i.getMessage("Active")).create()));
                                player.spigot().sendMessage(textComponent);
                            }
                        }
                    }
                    break;
                default:
                    String subCommand = args[0];
                    for (ParticleItem particleItem : ParticleTrails.i.getParticleItemList()){
                        if (particleItem.getCommands().contains(subCommand.toLowerCase())){
                            if (ParticleTrails.i.getParticlePlayer(player.getUniqueId()).hasEffectActive(particleItem))
                                particlePlayer.removeParticleEffect(particleItem.getParticleType());
                            else {
                                if (player.hasPermission(particleItem.getPermission())) {
                                    if (particlePlayer.canActivateEffect()) {
                                        particlePlayer.startParticleEffect(particleItem.getParticleType());
                                    } else {
                                        player.sendMessage(MessageManager.i.getMessage("ToManyParticles", particleItem.getName()));
                                    }
                                }else{
                                    if (ParticleTrails.i.isSoundEnabled())
                                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_LAND,1,1);
                                    player.sendMessage(MessageManager.i.getMessage("NoPermission"));
                                }
                                particlePlayer.startParticleEffect(particleItem.getParticleType());
                            }
                            return false;
                        }
                    }
                    if (commandValues == null)
                        reloadParticleSubCommands();
                    CommandAssist.commandCompare(player, "particle", commandValues, args);
            }
        }

        return false;
    }

    static char bullet = '\u25B6';
    private void createHelpMenu(){
        List<String> cmd = Arrays.asList("inv", "aura", "feet", "[head/halo]", "trail", "[off/end]", "toggle", "help", "list", "{effect}");
        List<String> info = Arrays.asList("Opens the particle inventory!","Forms particles from within you!",
                "Forms particles at your feet!","Forms particles above your head!","Forms particles behind you!",
                "Turns off all of your active effects!", "Toggles whether or not you see particles", "Shows this help menu!",
                "Show all cmds/effects you have access to!", "Activates specified effect!");
        TextComponent header = new TextComponent("");
        header.addExtra(new TextComponent(new ComponentBuilder("=+=+=+=+=+=+=+=+=").color(ChatColor.DARK_AQUA).bold(true).create()));
        header.addExtra(new TextComponent(new ComponentBuilder(" Particle Trails ").color(ChatColor.RED).create()));
        header.addExtra(new TextComponent(new ComponentBuilder("=+=+=+=+=+=+=+=+=").color(ChatColor.DARK_AQUA).bold(true).create()));
        helpList.add(header);
        for (int x = 0; x < cmd.size(); x++){
            ComponentBuilder componentBuilder = new ComponentBuilder("")
                    .append("/particle "+cmd.get(x)).color((x%2==0?ChatColor.GREEN : ChatColor.DARK_GREEN))
                    .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/particle "+(cmd.get(x).contains("/")
                            ? cmd.get(x).substring(1,cmd.get(x).length()-1).split("/")[0] : cmd.get(x).replace("{effect}"," "))))
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click Me!").color(ChatColor.AQUA).create()));
            TextComponent message = new TextComponent("");
            message.addExtra(new TextComponent(new ComponentBuilder("      "+bullet + " ").color(ChatColor.BLACK).create()));
            message.addExtra(new TextComponent(componentBuilder.create()));
            message.addExtra(new TextComponent(new ComponentBuilder("").append(" - ").color(ChatColor.WHITE)
                    .append(info.get(x)).color((x%2==0 ? ChatColor.YELLOW : ChatColor.GOLD)).create()));
            helpList.add(message);
        }

        List<String> adminCmd = Arrays.asList("demo","off {player}","reload", "list {player}");
        List<String> adminInfo = Arrays.asList("Shows demo for each of your effects","Turns off all effects for target!",
                "Reloads config, restarts effects! (BUGGY)", "Lists active effects for target!");
        for (int x = 0; x < adminCmd.size(); x++){
            ComponentBuilder componentBuilder = new ComponentBuilder("")
                    .append("/particle "+adminCmd.get(x)).color((x%2==0?ChatColor.RED : ChatColor.DARK_RED))
                    .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/particle "+(adminCmd.get(x).contains("/")
                            ? adminCmd.get(x).substring(1,adminCmd.get(x).length()-1).split("/")[0] : adminCmd.get(x)).replace("{player}"," ")))
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click Me!").color(ChatColor.AQUA).create()));
            TextComponent message = new TextComponent("");
            message.addExtra(new TextComponent(new ComponentBuilder(bullet + " ").color(ChatColor.DARK_PURPLE).create()));
            message.addExtra(new TextComponent(componentBuilder.create()));
            message.addExtra(new TextComponent(new ComponentBuilder("").append(" - ").color(ChatColor.WHITE)
                    .append(adminInfo.get(x)).color((x%2==0 ? ChatColor.BLUE : ChatColor.AQUA)).create()));
            adminList.add(message);
        }

    }

    public void sendHelpMenu(Player player){
        for (TextComponent textComponent : helpList)
            player.spigot().sendMessage(textComponent);
        if (player.hasPermission("Particle.admin"))
            sendAdminHelpMenu(player);
    }

    private void sendAdminHelpMenu(Player player){
        player.sendMessage("");
        player.sendMessage(ChatColor.GRAY.toString() + ChatColor.ITALIC + "Only players with \"Particle.admin\" will see the below!");
        for (TextComponent textComponent : adminList)
            player.spigot().sendMessage(textComponent);
    }

    public String getValidArgument(String s){
        if (s.equalsIgnoreCase("halo"))
            return "head";
        if (s.equalsIgnoreCase("end"))
            return "off";
        if (s.equalsIgnoreCase("show"))
            return "list";
        return s;
    }

    public TextComponent getArgsFromList(List<String> list){
        TextComponent textComponent = new TextComponent("");
        String s = "";
        for (int x = 0; x < list.size(); x++){
            if (x > 0)
                s=",";
            s+= " "+list.get(x);
            ComponentBuilder componentBuilder = new ComponentBuilder(s)
                    .color(ChatColor.GRAY)
                    .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/particle "+list.get(x)))
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click me!").color(ChatColor.LIGHT_PURPLE).create()));
            textComponent.addExtra(new TextComponent(componentBuilder.create()));
        }
        return textComponent;
    }

    static String[] defaultCommands = new String[]{"inv","aura","feet","head","trail","off","help","list","halo","end"};
    private void reloadParticleSubCommands(){
        if (commandValues == null)
            commandValues = new ArrayList<>();
        commandValues.clear();
        for (String s : defaultCommands)
            commandValues.add(s);
        for (ParticleItem particleItem : ParticleTrails.i.getParticleItemList())
            commandValues.addAll(particleItem.getCommands());
    }

    private void startDemo(Player player){
        player.sendMessage(ChatColor.RED + "Starting demo...");
        final List<ParticleItem> list = ParticleTrails.i.getParticleItemList();
        new BukkitRunnable(){
            int x = list.size();
            int timer = 0;
            @Override
            public void run() {
                if (player == null || !player.isOnline()){
                    this.cancel();
                    return;
                }

                if (timer <= 0){
                    x--;
                    if (x < 0){
                        player.sendMessage(ChatColor.RED + "End of demo!");
                        this.cancel();
                        return;
                    }
                    timer = 4*20;
                    player.sendMessage("EffectName: " + list.get(x).getDisplayName());
                }
                Block b = player.getTargetBlock((Set)null, 2);
                list.get(x).getParticleEffect().sendToLocation(b.getLocation().add(.5,0,.5));
                timer--;
            }
        }.runTaskTimer(ParticleTrails.i, 1, 1);
    }
}
