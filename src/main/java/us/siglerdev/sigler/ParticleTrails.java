package us.siglerdev.sigler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import org.bukkit.scheduler.BukkitRunnable;
import us.siglerdev.sigler.cmds.ParticleCommand;
import us.siglerdev.sigler.listeners.PlayerInteract;
import us.siglerdev.sigler.manager.InventoryManager;
import us.siglerdev.sigler.manager.MessageManager;
import us.siglerdev.sigler.manager.PlayerManager;
import us.siglerdev.sigler.manager.TrailRepository;
import us.siglerdev.sigler.particle.ParticleEffect;
import us.siglerdev.sigler.particle.ParticleItem;
import us.siglerdev.sigler.utils.FakeEnchant;

public class ParticleTrails extends JavaPlugin
{
    public static ParticleTrails i;
    public static FakeEnchant fakeEnchant = new FakeEnchant(69);
    private boolean mysql, sounds;
    private TrailRepository trailRepository;
    private File dataFile, configFile;
    public YamlConfiguration dataConfig, config;
    private PlayerManager playerManager;
    private InventoryManager inventoryManager;
    private MessageManager messageManager;
    private List<ParticleItem> particleItemList;
    private List<UUID> toggled;
    private int tickDelay = 1;
    public void onEnable(){
        i = this;

        /*  FlatFile values  */
        configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists())
            saveDefaultConfig();
        config = YamlConfiguration.loadConfiguration(configFile);
        dataFile = new File(getDataFolder(), "data.yml");
        dataConfig = YamlConfiguration.loadConfiguration(dataFile);
        sounds = config.getBoolean("PlaySounds");
        tickDelay = config.getInt("IntervalDelay");
        toggled = new ArrayList<>();

        /*  MySQL */
        mysql = (getServer().getPluginManager().isPluginEnabled("MySQLManager") && getConfig().getBoolean("MySQL.Enabled"));
        if (mysql)
            initMySQL();
        else if (getConfig().getBoolean("MySQL.Enabled")){
            getLogger().severe("You have MySQL enabled but you do NOT have MySQL Manager on this server. Please get it from "+
            "https://www.spigotmc.org/resources/mysql-manager.17732/");
        }

        /*  Initialization  */
        particleItemList = new ArrayList<>();
        playerManager = new PlayerManager();
        messageManager = new MessageManager(config);
        inventoryManager = new InventoryManager();

        /*  Listeners  */
        getServer().getPluginManager().registerEvents(playerManager, this);
        getServer().getPluginManager().registerEvents(inventoryManager, this);
        getServer().getPluginManager().registerEvents(new PlayerInteract(), this);

        /*  Commands  */
        getCommand("particle").setExecutor(new ParticleCommand());

        loadParticleItems();
    }

    public void initMySQL(){
        String hostname, port, database, username, password;
        hostname = getConfig().getString("MySQL.Hostname");
        port = getConfig().getString("MySQL.Port");
        database = getConfig().getString("MySQL.Database");
        username = getConfig().getString("MySQL.Username");
        password = getConfig().getString("MySQL.Password");
        MySQLManager.i.updateConnectionInfo(hostname,port,database,username,password); //Updates the connection information and reloads the connections
        trailRepository = new TrailRepository(); //Means MySQL is enabled and we can go through with it
    }

    public void reloadParticleItems(){
        configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists())
            saveDefaultConfig();
        config = YamlConfiguration.loadConfiguration(configFile);
        getLogger().info("Starting reload...");
        getLogger().info("Clearing particle items...");
        particleItemList.clear();
        getLogger().info("Re-adding particle items...");
        loadParticleItems();
        getLogger().info("Restarting all effects...");
        for (Player p : Bukkit.getOnlinePlayers())
            getParticlePlayer(p.getUniqueId()).restart();
        getLogger().info("Reload Complete!");
    }

    private void loadParticleItems(){
        for (String s : config.getConfigurationSection("Data").getKeys(false)){
            ParticleItem pi = new ParticleItem(ParticleEffect.ParticleType.valueOf(s),
                    config.getString("Data."+s+".DisplayName"),
                    config.getString("Data."+s+".ItemID"),
                    (config.contains("Data."+s+".Lore") ? config.getStringList("Data."+s+".Lore") : MessageManager.i.getStringList("Messages.NoPermLore")),
                    config.getStringList("Data."+s+".Command"),
                    config.getDouble("Data."+s+".Radius"),
                    config.getDouble("Data."+s+".Speed"),
                    config.getInt("Data."+s+".Count")
            );
            particleItemList.add(pi);
            //getLogger().info("ParticleItem: " + pi.getParticleType() + "  " + pi.getDisplayName()
            //        + "  " + pi.getItemId() + "  " + pi.getLore()+ "  " + pi.getCommands()+ "  "
            //       + pi.getRadius()+ "  " + pi.getSpeed()+ "  " + pi.getCount());
        }
    }

    public int getTickDelay(){
        return tickDelay;
    }

    public TrailRepository getTrailRepository(){
        return trailRepository;
    }

    public boolean isMySQLEnabled(){
        return mysql;
    }

    public boolean isSoundEnabled(){
        return sounds;
    }

    public void saveCustomConfig(){
        try
        {
            dataConfig.save(dataFile);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public List<UUID> getToggled(){
        return toggled;
    }

    public InventoryManager getInventoryManager(){
        return inventoryManager;
    }

    public ParticlePlayer getParticlePlayer(UUID id){
        return playerManager.getParticlePlayer(id);
    }

    public ParticleItem getParticleItem(ParticleEffect.ParticleType particleType){
        for (ParticleItem particleItem : particleItemList)
            if (particleItem.getParticleType() == particleType)
                return particleItem;
        return null;
    }

    public ParticleItem getParticleItem(String displayName){
        displayName = ChatColor.translateAlternateColorCodes('&',displayName);
        for (ParticleItem particleItem : particleItemList) {
            String particleItemName = particleItem.getDisplayName();
            if (particleItemName.equalsIgnoreCase(displayName))
                return particleItem;
            if (ChatColor.stripColor(particleItemName).equalsIgnoreCase(ChatColor.stripColor(displayName)))
                return particleItem;
        }
        return null;
    }

    public List<ParticleItem> getParticleItemList(){
        return particleItemList;
    }

}
