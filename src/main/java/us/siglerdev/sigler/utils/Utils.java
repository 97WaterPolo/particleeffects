package us.siglerdev.sigler.utils;

import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Alexander on 1/27/2016.
 */
public class Utils {
    public static void clearItemStack(ItemStack is){
        ItemMeta im = is.getItemMeta();
        im.setLore(null);
        im.setDisplayName(null);
        is.setItemMeta(im);
        is.setAmount(1);
        is.setDurability((short)0);
    }
    public static BlockFace getDirection(Player player) {
        double rotation = (player.getLocation().getYaw() - 180) % 360;
        if (rotation < 0)
            rotation += 360.0;
        if (0 <= rotation && rotation < 40.0)
            return BlockFace.NORTH;
        else if (40.0 <= rotation && rotation < 80.0)
            return BlockFace.NORTH_EAST;
        else if (80.0 <= rotation && rotation < 120.0)
            return BlockFace.EAST;
        else if (120.0 <= rotation && rotation < 160.0)
            return BlockFace.SOUTH_EAST;
        else if (160.0 <= rotation && rotation < 200.0)
            return BlockFace.SOUTH;
        else if (200.0 <= rotation && rotation < 240.0)
            return BlockFace.SOUTH_WEST;
        else if (240.0 <= rotation && rotation < 280.0)
            return BlockFace.WEST;
        else if (280.0 <= rotation && rotation < 320.0)
            return BlockFace.NORTH_WEST;
        else if (320.0 <= rotation && rotation < 360.0)
            return BlockFace.NORTH;
        else
            return null;
    }
}
