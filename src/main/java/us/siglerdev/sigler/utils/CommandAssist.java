package us.siglerdev.sigler.utils;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Created by Alexander on 1/30/2016.
 */
public class CommandAssist {
    public static void commandCompare(Player player, String cmdName, List<String> subommands, String[] input)
    {
        HashMap<String, Float> cmd_results = new HashMap<>();
        Iterator<String> commands = subommands.iterator();
        String subcmd = input[0];
        while (commands.hasNext())
        {
            String cmd = commands.next();
            cmd_results.put(cmd, averageMatches(subcmd, cmd));
        }

        List<String> approx = sort(cmd_results);
        if(approx.size() > 1)
        {
            String args = "";
            for(int i = 1; i < input.length; ++i)
                args += " " + input[i];
            player.sendMessage(ChatColor.DARK_RED + "Did you mean: ");
            approx.stream().forEach(e -> {
                TextComponent textComponent = new TextComponent(
                        new ComponentBuilder("/particle "+ e).color(ChatColor.RED)
                                .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/particle "+ e))
                                .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                        new ComponentBuilder("Click Me!").color(ChatColor.LIGHT_PURPLE).create())).create());
                player.spigot().sendMessage(textComponent);
            });
        }
        else if (approx.size() == 1)
        {
            String cmd = "particle " + approx.get(0);
            player.performCommand(cmd);
        }
        else
            player.performCommand("particle help");
    }

    private static float averageMatches(String input, String command)
    {
        int matches;
        int i_length = input.length();
        int c_length = command.length();
        int[] results = new int[2];

        for(int o = 0; o < 2; ++o)
        {
            matches = 0;
            if(c_length < i_length)
                matches = (c_length - i_length);
            for (int i = 0; (i + o) < i_length && i < c_length; ++i)
            {
                if (input.charAt(i + o) == command.charAt(i))
                    ++matches;
            }
            results[o] = matches;
        }
        return average(results);
    }

    private static float average(int[] values)
    {
        float avg = 0;

        for(int value : values)
            avg += value;

        return (avg/values.length);
    }

    private static List<String> sort(HashMap<String, Float> values)
    {
        final List<String> matches = new ArrayList<>();
        float max = values.values().stream().findFirst().get();

        for (Map.Entry<String, Float> entry : values.entrySet())
        {
            String key = entry.getKey();
            float value = entry.getValue();
            if (value > max && value != 0)
            {
                max = value;
                matches.clear();
                matches.add(key);
            }
            else if (value == max && value != 0)
                matches.add(key);
        }

        return matches;
    }
}
