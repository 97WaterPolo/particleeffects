package us.siglerdev.sigler.utils;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/**
 * Created by Alexander on 1/26/2016.
 */
public class Items {

    public static ItemStack
            ON_HELMET = getArmorItemStack(Material.LEATHER_HELMET, Value.ON),
            ON_CHESTPLATE = getArmorItemStack(Material.LEATHER_CHESTPLATE, Value.ON),
            ON_LEGGINGS = getArmorItemStack(Material.LEATHER_LEGGINGS, Value.ON),
            ON_BOOTS = getArmorItemStack(Material.LEATHER_BOOTS, Value.ON),
            OFF_HELMET = getArmorItemStack(Material.LEATHER_HELMET, Value.OFF),
            OFF_CHESTPLATE = getArmorItemStack(Material.LEATHER_CHESTPLATE, Value.OFF),
            OFF_LEGGINGS = getArmorItemStack(Material.LEATHER_LEGGINGS, Value.OFF),
            OFF_BOOTS = getArmorItemStack(Material.LEATHER_BOOTS, Value.OFF),
            DISABLED_LEGGINGS = setDisplayName(getArmorItemStack(Material.LEATHER_LEGGINGS, Value.OFF), "&f"),
            GLASS = setDisplayName(new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte)8), "&f"),
            CLEAR = setDisplayName(new ItemStack(Material.REDSTONE_BLOCK), ChatColor.DARK_RED + "Clear Effects");

    private static ItemStack getArmorItemStack(Material m, Value value){
        ItemStack is = new ItemStack(m);
        LeatherArmorMeta lam = (LeatherArmorMeta) is.getItemMeta();
        if (value == Value.ON)
            lam.setColor(Color.fromRGB(255,0,0));
        else if (value == Value.OFF)
            lam.setColor(Color.fromRGB(0,255,0));
        else if (value == Value.DISABLED)
            lam.setColor(Color.fromRGB(0,0,0));
        is.setItemMeta(lam);
        return is;
    }

    private static ItemStack setDisplayName(ItemStack is, String name){
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        is.setItemMeta(im);
        return is;
    }

    enum Value{
        ON,OFF,DISABLED;
    }

}
