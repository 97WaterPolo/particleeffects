package us.siglerdev.sigler.utils;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Location;
import us.siglerdev.sigler.ParticleTrails;
import us.siglerdev.sigler.manager.MessageManager;

public enum ParticleLocation
{
	HEAD,FEET,TRAIL,AURA;

	public double[] getLocation(){
		double[] vals = new double[]{0,0,0};
		double x = ParticleTrails.i.config.getDouble("Offsets."+ WordUtils.capitalize(this.toString().toLowerCase()) + ".X");
		double y = ParticleTrails.i.config.getDouble("Offsets."+ WordUtils.capitalize(this.toString().toLowerCase()) + ".Y");
		double z = ParticleTrails.i.config.getDouble("Offsets."+ WordUtils.capitalize(this.toString().toLowerCase()) + ".Z");
		vals[0] = x;
		vals[1] = y;
		vals[2] = z;
		return vals;
	}

}
