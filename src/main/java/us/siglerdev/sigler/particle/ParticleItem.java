package us.siglerdev.sigler.particle;

import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import us.siglerdev.sigler.ParticleTrails;
import us.siglerdev.sigler.manager.MessageManager;
import us.siglerdev.sigler.utils.FakeEnchant;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Alexander on 1/27/2016.
 */
public class ParticleItem {
    private double radius,speed,count;
    private String displayName, itemId;
    private List<String> lore, commands;
    private ParticleEffect particleEffect;
    private ParticleEffect.ParticleType particleType;

    public ParticleItem(ParticleEffect.ParticleType particleType, String displayName,
                        String itemId, List<String> lore, List<String> commands, double... val){
        this.displayName = displayName;
        this.itemId = itemId;
        this.lore = lore;
        this.particleType = particleType;
        this.commands = commands;
        for (int x = 0; x < commands.size(); x++)
            commands.set(x, commands.get(x).toLowerCase());
        this.radius = val[0];
        this.speed = val[1];
        this.count = val[2];
        this.particleEffect = new ParticleEffect(particleType, radius, speed, (int)count);

        for (int x =0 ; x < lore.size(); x++)
            lore.set(x, ChatColor.translateAlternateColorCodes('&',lore.get(x)));
    }

    public String getPermission(){
        return "Particle.effect."+particleType.toString().toLowerCase();
    }

    public String getItemId(){
        return itemId;
    }

    public double getRadius(){
        return radius;
    }

    public double getSpeed(){
        return speed;
    }

    public int getCount() {
        return (int)count;
    }

    public String getDisplayName(){
        return ChatColor.translateAlternateColorCodes('&',displayName);
    }

    public String getName(){
        return WordUtils.capitalize(particleType.toString().toLowerCase().replace("_"," "));
    }

    public List<String> getLore(){
        return lore;
    }

    public List<String> getCommands(){
        return commands;
    }

    public ParticleEffect.ParticleType getParticleType(){
        return particleType;
    }

    public ParticleEffect getParticleEffect(){
        return particleEffect;
    }

    public ItemStack getItemstack(boolean hasPerm, boolean isActive){
        ItemStack is = null;
        if (itemId.contains(":"))
            is = new ItemStack(Material.getMaterial(Integer.valueOf(itemId.split(":")[0])), 1, Byte.valueOf(itemId.split(":")[1]));
        else
            is = new ItemStack(Material.getMaterial(Integer.valueOf(itemId.split(":")[0])));
        is.setAmount(1);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(getDisplayName());
        if (!hasPerm)
            im.setLore(getLore());
        if (isActive){
            if (is.getItemMeta().hasLore())
                im.getLore().add(MessageManager.i.getMessage("Active"));
            else
                im.setLore(Arrays.asList(MessageManager.i.getMessage("Active")));

        }
        is.setItemMeta(im);
        if (isActive)
            is.addUnsafeEnchantment(ParticleTrails.fakeEnchant,1);
        return is;
    }

}
