package us.siglerdev.sigler.particle;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.sigler.ParticleTrails;
import us.siglerdev.sigler.utils.ParticleLocation;
import us.siglerdev.sigler.utils.Utils;

public class Particle extends BukkitRunnable
{
	private Player player;
	private ParticleEffect pe;
	private ParticleLocation particleLocation;
	private double[] offsets;
	public Particle(UUID id, ParticleEffect pe, ParticleLocation particleLocation){
		this.player = Bukkit.getPlayer(id);
		this.pe = pe;
		this.particleLocation = particleLocation;
		offsets = particleLocation.getLocation();
		runTaskTimer(ParticleTrails.i,10,ParticleTrails.i.getTickDelay());
	}

	@Override
	public void run()
	{
		switch(particleLocation){
			case HEAD:
				pe.sendToLocation((pe.getType() == ParticleEffect.ParticleType.MOB_APPEARANCE ? player : null),
						player.getLocation().add(0+offsets[0],2.5+offsets[1],0+offsets[2]));
				break;
			case FEET:
				pe.sendToLocation((pe.getType() == ParticleEffect.ParticleType.MOB_APPEARANCE ? player : null),
						player.getLocation().add(0+offsets[0],.1+offsets[1],0+offsets[2]));
				break;
			case TRAIL:
				pe.sendToLocation(player.getLocation().clone().add(0,.1,0).subtract(player.getLocation().getDirection()));
				//pe.sendToLocation((pe.getType() == ParticleEffect.ParticleType.MOB_APPEARANCE ? player : null),
				//		player.getLocation().getBlock().getRelative(Utils.getDirection(player).getOppositeFace())
				//		.getLocation().add(0.5+offsets[0], 0.25+offsets[1], 0.5+offsets[2]));
				break;
			case AURA:
				pe.sendToLocation((pe.getType() == ParticleEffect.ParticleType.MOB_APPEARANCE ? player : null),
						player.getLocation().add(0+offsets[0],1.15+offsets[1],0+offsets[2]));
				break;
			default:
				player = null;
				this.cancel();
				return;
		}
		if (player == null || !player.isOnline())
			this.cancel();

	}

	public String getType(){
		return pe.getType().toString();
	}

	public ParticleLocation getParticleLocation(){
		return particleLocation;
	}

	public void updateParticleLocation(ParticleLocation particleLocation){
		this.particleLocation = particleLocation;
		offsets = particleLocation.getLocation();
	}


}
