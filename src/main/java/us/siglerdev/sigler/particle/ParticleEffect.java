package us.siglerdev.sigler.particle;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import net.minecraft.server.v1_9_R1.EnumParticle;
import net.minecraft.server.v1_9_R1.PacketPlayOutWorldParticles;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import us.siglerdev.sigler.ParticleTrails;

/**
 * Particle effects utility library
 * @author Maxim Roncace
 * @version 0.1.0
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ParticleEffect {

	private static Class<?> packetClass = null;
	private static Constructor<?> packetConstructor = null;
	private static Field[] fields = null;
	private static boolean netty = true;
	private static Field player_connection = null;
	private static Method player_sendPacket = null;
	private static HashMap<Class<? extends Entity>, Method> handles = new HashMap<Class<? extends Entity>, Method>();

	private static boolean newParticlePacketConstructor = false;
	private static Class<Enum> enumParticle = null;

	private ParticleType type;
	private double speed;
	private int count;
	private double radOne, radTwo, radThree;

	private static boolean compatible = true;

	static {
		String vString = getVersion().replace("v", "");
		double v = 0;
		if (!vString.isEmpty()){
			String[] array = vString.split("_");
			v = Double.parseDouble(array[0] + "." + array[1]);
		}
		try {
			Bukkit.getLogger().info("[ParticleLib] Server major/minor version: " + v);
			if (v < 1.7) {
				Bukkit.getLogger().info("[ParticleLib] Hooking into pre-Netty NMS classes");
				netty = false;
				packetClass = getNmsClass("Packet63WorldParticles");
				packetConstructor = packetClass.getConstructor();
				fields = packetClass.getDeclaredFields();
			}
			else {
				Bukkit.getLogger().info("[ParticleLib] Hooking into Netty NMS classes");
				packetClass = getNmsClass("PacketPlayOutWorldParticles");
				if (v < 1.8){
					Bukkit.getLogger().info("[ParticleLib] Version is < 1.8 - using old packet constructor");
					packetConstructor = packetClass.getConstructor(String.class, float.class, float.class, float.class,
							float.class, float.class, float.class, float.class, int.class);
				}
				else{ // use the new constructor for 1.8
					Bukkit.getLogger().info("[ParticleLib] Version is >= 1.8 - using new packet constructor");
					newParticlePacketConstructor = true;
					enumParticle = (Class<Enum>)getNmsClass("EnumParticle");
					packetConstructor = packetClass.getDeclaredConstructor(enumParticle, boolean.class, float.class,
							float.class, float.class, float.class, float.class, float.class, float.class, int.class,
							int[].class);
				}
			}
		}
		catch (Exception ex){
			ex.printStackTrace();
			Bukkit.getLogger().severe("[ParticleLib] Failed to initialize NMS components!");
			compatible = false;
		}
	}

	/**
	 * Constructs a new particle effect for use.
	 * <p>
	 *     Note: different values for speed and radius may hav;e different effects
	 *     depending on the particle's type.
	 * </p>
	 * @param type the particle type
	 * @param speed the speed of the particles
	 * @param count the number of particles to spawn
	 * @param radius the radius of the particles
	 */
	public ParticleEffect(ParticleType type,double radius, double speed, int count){
		this.type = type;
		this.speed = speed;
		this.count = count;
		this.radOne = radius;
		this.radTwo = radius;
		this.radThree = radius;
	}

	public ParticleEffect(ParticleType type,double radOne, double radTwo, double radThree, double speed, int count){
		this.type = type;
		this.speed = speed;
		this.count = count;
		this.radOne = radOne;
		this.radTwo = radTwo;
		this.radThree = radThree;
	}

	public ParticleType getType(){
		return type;
	}

	/**
	 * Gets the speed of the particles in this effect
	 * @return The speed of the particles in this effect
	 */
	public double getSpeed(){
		return speed;
	}

	/**
	 * Retrieves the number of particles spawned by the effect
	 * @return The number of particles spawned by the effect
	 */
	public int getCount(){
		return count;
	}

	/**
	 * Gets the radius of the particle effect
	 * @return The radius of the particle effect
	 */
	public double getRadius(){
		return radOne;
	}

	/**
	 * Send a particle effect to all players
	 * @param location The location to send the effect to
	 */
	public void sendToLocation(Location location){
		try {
			Object packet = createPacket(location);
			for (Player player : location.getWorld().getPlayers()){
				if (ParticleTrails.i.getToggled().contains(player.getUniqueId()))
					continue;
				sendPacket(player, packet);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	public void sendToLocation(Player player, Location location){
		try {
			Object packet = createPacket(location);
			if (player != null)
				sendPacket(player, packet);
			else
				sendToLocation(location);
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Constructs a new particle packet.
	 * @param location the location to spawn the particle effect at
	 * @return the constructed packet
	 */
	private Object createPacket(Location location){
		try {
			if (this.count <= 0){
				this.count = 1;
			}
			Object packet;
			if (netty){
				if (newParticlePacketConstructor){
					Object particleType = enumParticle.getEnumConstants()[type.getId()];
					packet = packetConstructor.newInstance(particleType,
							true, (float)location.getX(), (float)location.getY(), (float)location.getZ(),
							(float)this.radOne, (float)this.radTwo, (float)this.radThree,
							(float)this.speed, this.count, new int[0]);
				}
				else {
					packet = packetConstructor.newInstance(type.getName(),
							(float)location.getX(), (float)location.getY(), (float)location.getZ(),
							(float)this.radOne, (float)this.radTwo, (float)this.radThree,
							(float)this.speed, this.count);
				}
			}
			else {
				packet = packetConstructor.newInstance();
				for (Field f : fields){
					f.setAccessible(true);
					if (f.getName().equals("a"))
						f.set(packet, type.getName());
					else if (f.getName().equals("b"))
						f.set(packet, (float)location.getX());
					else if (f.getName().equals("c"))
						f.set(packet, (float)location.getY());
					else if (f.getName().equals("d"))
						f.set(packet, (float)location.getZ());
					else if (f.getName().equals("e"))
						f.set(packet, this.radOne);
					else if (f.getName().equals("f") )
						f.set(packet, this.radTwo);
					else if (f.getName().equals("g"))
						f.set(packet, this.radThree);
					else if (f.getName().equals("h"))
						f.set(packet, this.speed);
					else if (f.getName().equals("i"))
						f.set(packet, this.count);
				}
			}
			return packet;
		}
		catch (IllegalAccessException ex){
			ex.printStackTrace();
			Bukkit.getLogger().severe("{ParticleLib] Failed to construct particle effect packet!");
		}
		catch (InstantiationException ex){
			ex.printStackTrace();
			Bukkit.getLogger().severe("{ParticleLib] Failed to construct particle effect packet!");
		}
		catch (InvocationTargetException ex){
			ex.printStackTrace();
			Bukkit.getLogger().severe("{ParticleLib] Failed to construct particle effect packet!");
		}
		return null;
	}

	/**
	 * Sends a packet to a player.
	 * <p>
	 *     Note: this method is <strong>not typesafe</strong>!
	 * </p>
	 * @param p the player to send a packet to
	 * @param packet the packet to send
	 * @throws IllegalArgumentException if <code>packet</code> is not of a proper type
	 */
	private static void sendPacket(Player p, Object packet) throws IllegalArgumentException {
		try {
			if (player_connection == null){
				player_connection = getHandle(p).getClass().getField("playerConnection");
				for (Method m : player_connection.get(getHandle(p)).getClass().getMethods()){
					if (m.getName().equalsIgnoreCase("sendPacket")){
						player_sendPacket = m;
					}
				}
			}
			player_sendPacket.invoke(player_connection.get(getHandle(p)), packet);
		}
		catch (IllegalAccessException ex){
			ex.printStackTrace();
			Bukkit.getLogger().severe("[ParticleLib] Failed to send packet!");
		}
		catch (InvocationTargetException ex){
			ex.printStackTrace();
			Bukkit.getLogger().severe("[ParticleLib] Failed to send packet!");
		}
		catch (NoSuchFieldException ex){
			ex.printStackTrace();
			Bukkit.getLogger().severe("[ParticleLib] Failed to send packet!");
		}
	}

	/**
	 * Gets the NMS handle of the given {@link Entity}.
	 * @param entity the entity get the handle of
	 * @return the entity's NMS handle
	 */
	private static Object getHandle(Entity entity){
		try {
			if (handles.get(entity.getClass()) != null)
				return handles.get(entity.getClass()).invoke(entity);
			else {
				Method entity_getHandle = entity.getClass().getMethod("getHandle");
				handles.put(entity.getClass(), entity_getHandle);
				return entity_getHandle.invoke(entity);
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * Gets the NMS class by the given name.
	 * @param name the name of the NMS class to get
	 * @return the NMS class of the given name
	 */
	private static Class<?> getNmsClass(String name){
		String version = getVersion();
		String className = "net.minecraft.server." + version + name;
		Class<?> clazz = null;
		try {
			clazz = Class.forName(className);
		}
		catch (ClassNotFoundException ex){
			ex.printStackTrace();
			Bukkit.getLogger().severe("[ParticleLib] Failed to load NMS class " + name + "!");
		}
		return clazz;
	}

	/**
	 * Determines the version string used by Craftbukkit's safeguard (e.g. 1_7_R4).
	 * @return the version string used by Craftbukkit's safeguard
	 */
	private static String getVersion(){
		String[] array = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",");
		if (array.length == 4)
			return array[3] + ".";
		return "";
	}

	/**
	 * Gets whether ParticleLib is compatible with the server software.
	 * @return whether ParticleLib is compatible with the server software.
	 */
	public static boolean isCompatible(){
		return compatible;
	}

	/**
	 * Enum representing valid particle types in Minecraft 1.8
	 */
	public enum ParticleType {
		EXPLOSION_NORMAL("explode", 0, true),
		EXPLOSION_LARGE("largeexplode", 1, true),
		EXPLOSION_HUGE("hugeexplosion", 2, true),
		FIREWORKS_SPARK("fireworksSpark", 3, false),
		WATER_BUBBLE("bubble", 4, false),
		WATER_SPLASH("splash", 5, false),
		WATER_WAKE("wake", 6, false),
		SUSPENDED("suspended", 7, false),
		SUSPENDED_DEPTH("depthsuspend", 8, false),
		CRIT("crit", 9, false),
		CRIT_MAGIC("magicCrit", 10, false),
		SMOKE_NORMAL("smoke", 11, false),
		SMOKE_LARGE("largesmoke", 12, false),
		SPELL("spell", 13, false),
		SPELL_INSTANT("instantSpell", 14, false),
		SPELL_MOB("mobSpell", 15, false),
		SPELL_MOB_AMBIENT("mobSpellAmbient", 16, false),
		SPELL_WITCH("witchMagic", 17, false),
		DRIP_WATER("dripWater", 18, false),
		DRIP_LAVA("dripLava", 19, false),
		VILLAGER_ANGRY("angryVillager", 20, false),
		VILLAGER_HAPPY("happyVillager", 21, false),
		TOWN_AURA("townaura", 22, false),
		NOTE("note", 23, false),
		PORTAL("portal", 24, false),
		ENCHANTMENT_TABLE("enchantmenttable", 25, false),
		FLAME("flame", 26, false),
		LAVA("lava", 27, false),
		FOOTSTEP("footstep", 28, false),
		CLOUD("cloud", 29, false),
		REDSTONE("reddust", 30, false),
		SNOWBALL("snowballpoof", 31, false),
		SNOW_SHOVEL("snowshovel", 32, false),
		SLIME("slime", 33, false),
		HEART("heart", 34, false),
		BARRIER("barrier", 35, false),
		//ITEM_CRACK("iconcrack", 36, false, 2),
		//BLOCK_CRACK("blockcrack", 37, false, 1),
		//BLOCK_DUST("blockdust", 38, false, 1),
		WATER_DROP("droplet", 39, false),
		//ITEM_TAKE("take", 40, false),
		MOB_APPEARANCE("mobappearance", 41, true),
		DRAGON_BREATH("dragonbreath", 42, false),
		END_ROD("endRod", 43, false),
		DAMAGE_INDICATOR("damageIndicator", 44, true),
		SWEEP_ATTACK("sweepAttack", 45, true);

		private String name;
		private int id;
		private boolean b;

		ParticleType(String name, int id, boolean b){
			this.name = name;
			this.id = id;
			this.b = b;
		}

		/**
		 * Gets the name of the particle effect
		 *
		 * @return The name of the particle effect
		 */
		String getName(){
			return name;
		}

		/**
		 * Gets the ID of the particle effect
		 *
		 * @return The ID of the particle effect
		 */
		int getId(){
			return id;
		}

		/**
		 * Gets the legacy ID (pre-1.8) of the particle effect
		 *
		 * @return the legacy ID of the particle effect (or -1 if introduced after 1.7)
		 */
	}

}